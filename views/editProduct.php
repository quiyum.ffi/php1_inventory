<?php
session_start();
include_once("templateLayout/templateInfo.php");
include_once "../src/Category.php";
include_once "../src/Unit_lookup.php";
include_once "../src/Product.php";
$categoryObj=new Category();
$allData=$categoryObj-> showCategory();
$unitObj=new Unit_lookup();
$unitData=$unitObj->showUnit();
$productObj=new Product();
$productObj->prepareData($_GET);
$productData=$productObj-> showProduct();
$oneProduct=$productObj->showOneProduct();



?>
<!DOCTYPE HTML>
<html>
<head>
    <title><?php echo $title;?></title>
    <?php include_once("templateLayout/css.php");?>
</head>
<body>
<div class="page-container">
    <div class="left-content">
        <?php include_once("templateLayout/header.php") ?>
        <!-- script-for sticky-nav -->
        <?php include_once("templateLayout/script.php") ?>
        <!-- /script-for sticky-nav -->
        <!--inner block start here-->
        <div class="inner-block" style="min-height: 700px">
            <div class="row">
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <div class="login-block">
                       <p class="text-center"><?php if(isset($_SESSION['message'])){echo $_SESSION['message']; $_SESSION['message']="";} ?></p>
                        <form action="../controller/updateProduct.php" method="post">
                            <input type="text" name="product_name" value="<?php echo $oneProduct->product_name?>" required="">
                            <input type="hidden" name="id" value="<?php echo $oneProduct->id;?>">
                            <br>
                            <select class="form-control" name="category_id">
                                <?php
                                foreach($allData as $oneData){
                                    ?>
                                    <option value='<?php echo $oneData->id; ?>' <?php if($oneProduct->category_id==$oneData->id){echo "selected";}?> ><?php echo $oneData->category_name; ?></option>
                                    <?php
                                }
                                ?>
                            </select>
                            <br>
                            <select class="form-control" name="unit_id">
                                <?php
                                foreach($unitData as $oneData){
                                    ?>
                                    <option value='<?php echo $oneData->id; ?>'  <?php if($oneProduct->unit_id==$oneData->id){echo "selected";}?>><?php echo $oneData->unit_name; ?></option>
                                <?php
                                }
                                ?>
                            </select>
                            <br>
                            <input type="submit" value="Update Product">
                        </form>
                    </div>
                </div>
                <div class="col-md-6 col-sm-6 col-xs-12" style="margin-top: 23px">
                    <h4 class="text-center">Category Lookup Table</h4><br>
                    <table class="table table-bordered table-responsive table-hover table-striped">
                        <thead>
                        <tr>
                            <th>Serial</th>
                            <th>Product Name</th>
                            <th>Category Name</th>
                            <th>Unit Name </th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        $serial=1;
                        foreach ($productData as $oneData){
                            echo "<tr>
                                        <td> $serial</td>
                                        <td> $oneData->product_name</td>
                                        <td> $oneData->category_name</td>
                                        <td> $oneData->unit_name</td>
                                        <td><a href='editProduct.php?id=$oneData->id' class='btn btn-primary'>Edit</a></td>
                                    </tr>";
                            $serial++;
                        }
                        ?>

                        </tbody>
                    </table>
                </div>
            </div>

        </div>
        <!--inner block end here-->
        <?php include_once("templateLayout/footer.php");?>
    </div>

    <!--slider menu-->
    <?php include_once("templateLayout/navigation.php");?>
    <div class="clearfix"> </div>
</div>
<!--slide bar menu end here-->
<?php include_once("templateLayout/script.php");?>
</body>
</html>




