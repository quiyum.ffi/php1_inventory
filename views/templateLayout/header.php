<div class="header-main">
    <div class="header-left">
        <div class="logo-name">
            <a href="index.html"> <h1>Inventory</h1>
                <!--<img id="logo" src="" alt="Logo"/>-->
            </a>
        </div>
        <!--search-box-->

        <div class="clearfix"> </div>
    </div>
    <div class="header-right">
        <div class="profile_details_left"><!--notifications of menu start -->
         
            <div class="clearfix"> </div>
        </div>
        <!--notification menu end -->
        <div class="profile_details">
            <ul>
                <li class="dropdown profile_details_drop">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                        <div class="profile_img">
                            <span class="prfil-img"><img src="images/p1.png" alt=""> </span>
                            <div class="user-name">
                                <p>Malorum</p>
                                <span>Administrator</span>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </a>
                </li>
                <li class="dropdown profile_details_drop">
                    <a href="<?php echo $base_url?>controller/logout.php" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                        <div class="profile_img">
                            <span class="prfil-img"><img src="images/p1.png" alt=""> </span>
                            <div class="user-name">
                                <p>Log Out</p>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </a>
                </li>
            </ul>
        </div>
        <div class="clearfix"> </div>
    </div>
    <div class="clearfix"> </div>
</div>
<!--heder end here-->
<!-- script-for sticky-nav -->
<script>
    $(document).ready(function() {
        var navoffeset=$(".header-main").offset().top;
        $(window).scroll(function(){
            var scrollpos=$(window).scrollTop();
            if(scrollpos >=navoffeset){
                $(".header-main").addClass("fixed");
            }else{
                $(".header-main").removeClass("fixed");
            }
        });

    });
</script>
<!-- /script-for sticky-nav -->
<!--inner block start here-->