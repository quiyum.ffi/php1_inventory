<div class="sidebar-menu">
    <div class="logo"> <a href="#" class="sidebar-icon"> <span class="fa fa-bars"></span> </a> <a href="#"> <span id="logo" ></span>
            <!--<img id="logo" src="" alt="Logo"/>-->
        </a> </div>
    <div class="menu">
        <ul id="menu" >
            <li id="menu-home" ><a href="<?php echo $base_url;?>/views/index.php"><i class="fa fa-tachometer"></i><span>Dashboard</span></a></li>
            <li><a href="<?php echo $base_url;?>/views/product.php""><i class="fa fa-cogs"></i><span>Product</span><span class="fa fa-angle-right" style="float: right"></span></a>
                <ul>
                    <li><a href="<?php echo $base_url;?>/views/product.php">Add Product</a></li>
                    <li><a href="<?php echo $base_url;?>/views/category.php">Add Category</a></li>
                    <li><a href="<?php echo $base_url;?>/views/unit.php">Add Unit</a></li>
                </ul>
            </li>
            <li id="menu-comunicacao" ><a href="#"><i class="fa fa-book nav_icon"></i><span>Purchase</span><span class="fa fa-angle-right" style="float: right"></span></a>
                <ul id="menu-comunicacao-sub" >
                    <li id="menu-arquivos" ><a href="purchase.php">Purchase Product</a></li>
                    <li id="menu-arquivos" ><a href="purchase_lookup.php">Purchase List</a></li>
                </ul>
            </li>

            <li id="menu-academico" ><a href="#"><i class="fa fa-file-text"></i><span>Sale</span><span class="fa fa-angle-right" style="float: right"></span></a>
                <ul id="menu-academico-sub" >
                    <li id="menu-academico-boletim" ><a href="bill.php">Sale Product</a></li>
                    <li id="menu-academico-avaliacoes" ><a href="bill_lookup.php">Sale List</a></li>
                </ul>
            </li>

            <li><a href="charts.html"><i class="fa fa-bar-chart"></i><span>Stock</span></a></li>

        </ul>
    </div>
</div>