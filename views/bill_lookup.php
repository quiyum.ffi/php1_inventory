<?php
include_once("templateLayout/templateInfo.php");
include_once "../src/Bill_master.php";
$bMasterObject=new Bill_master();
$bMasterObject->prepareData($_POST);
$paidList=$bMasterObject->showPaid();
$unPaidList=$bMasterObject->showUnPaid();

?>
<!DOCTYPE HTML>
<html>
<head>
    <title><?php echo $title;?></title>
    <?php include_once("templateLayout/css.php");?>
</head>
<body>
<div class="page-container">
    <div class="left-content">
        <?php include_once("templateLayout/header.php") ?>
        <!-- script-for sticky-nav -->
        <?php include_once("templateLayout/script.php") ?>
        <!-- /script-for sticky-nav -->
        <!--inner block start here-->

        <div class="row">
            <div class="col-md-10 col-md-offset-1 col-sm-10 col-sm-offset-1 col-xs-12">
                <div class="login-block">
                    <br><br>
                    <h1 class="text-center" style="color:green"> Paid List</h1>
                    <br><br>
                    <table id="paid" class="table table-bordered" >
                        <thead>
                        <tr>
                            <th>Serial</th>
                            <th>MRR No</th>
                            <th>Date</th>
                            <th>Time</th>
                            <th>Customer Name</th>
                            <th>Customer Contact</th>
                            <th>Total Price</th>
                            <th>Payment</th>
                            <th>Due</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        $serial=1;
                        foreach($paidList as $oneData){
                            $date=date("d-M-Y",strtotime($oneData->date));
                            $time=date("h:i a",strtotime($oneData->date));

                            echo   "<tr>
                                    <td>$serial</td>
                                    <td>$oneData->id</td>
                                    <td>$date</td>
                                    <td>$time</td>
                                    <td>$oneData->customer_name</td>
                                    <td>$oneData->customer_contact</td>
                                    <td>$oneData->total</td>
                                    <td>$oneData->payment</td>
                                    <td>$oneData->due</td>
                                    <td><a href='bill_details.php?id=$oneData->id' class='btn btn-warning'>Details</a></td>
                                </tr>";

                            $serial++;
                        }
                        ?>

                        </tbody>
                    </table> <br><br><br>

                    <h1 class="text-center" style="color:red"> Unpaid List</h1>
                    <br><br>
                    <table id="unpaid" class="table table-bordered" >
                        <thead>
                        <tr>
                            <th>Serial</th>
                            <th>MRR No</th>
                            <th>Date</th>
                            <th>Time</th>
                            <th>Customer Name</th>
                            <th>Customer Contact</th>
                            <th>Total Price</th>
                            <th>Payment</th>
                            <th>Due</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        $serial=1;
                        foreach($unPaidList as $oneData){
                            $date=date("d-M-Y",strtotime($oneData->date));
                            $time=date("h:i a",strtotime($oneData->date));
                            ?>
                            <tr>
                                <td><?php echo $serial;?></td>
                                <td><?php echo $oneData->id;?></td>
                                <td><?php echo $date?></td>
                                <td><?php echo $time;?></td>
                                <td><?php echo $oneData->customer_name;?></td>
                                <td><?php echo $oneData->customer_contact;?></td>
                                <td><?php echo $oneData->total;?></td>
                                <td><?php echo $oneData->payment;?></td>
                                <td><?php echo $oneData->due;?></td>
                                <td><a href='bill_details.php?id=<?php echo $oneData->id; ?>' class='btn btn-warning'>Details</a></td>
                            </tr>
                            <?php
                            $serial++;
                        }
                        ?>
                        </tbody>
                    </table> <br>
                </div>
            </div>
        </div>
        <!--inner block end here-->
        <?php include_once("templateLayout/footer.php");?>
    </div>

    <!--slider menu-->
    <?php include_once("templateLayout/navigation.php");?>
    <div class="clearfix"> </div>
</div>
<!--slide bar menu end here-->
<?php include_once("templateLayout/script.php");?>
</body>
</html>




