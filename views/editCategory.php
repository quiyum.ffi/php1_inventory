<?php
include_once("templateLayout/templateInfo.php");
include_once "../src/Category.php";
$object=new Category();
$object->prepareData($_GET);
$allData=$object-> showCategory();
$oneCategory=$object-> showOneCategory();
?>
    <!DOCTYPE HTML>
    <html>
    <head>
        <title><?php echo $title;?></title>
        <?php include_once("templateLayout/css.php");?>
    </head>
    <body>
    <div class="page-container">
        <div class="left-content">
            <?php include_once("templateLayout/header.php") ?>
            <!-- script-for sticky-nav -->
            <?php include_once("templateLayout/script.php") ?>
            <!-- /script-for sticky-nav -->
            <!--inner block start here-->
            <div class="inner-block" style="min-height: 700px">
                <div class="row">
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <div class="login-block">

                            <form action="../controller/updateCategory.php" method="post">
                                <input type="text" name="category_name" value="<?php echo $oneCategory->category_name;?>" required="">
                                <br>
                                <input type="text" name="category_desc" value="<?php echo $oneCategory->category_desc;?>" required="" >
                                <br>
                                <input type="hidden" name="id" value="<?php echo $oneCategory->id;?>">
                                <input type="submit" value="Update Category">
                            </form>
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-12" style="margin-top: 23px">
                        <h4 class="text-center">Category Lookup Table</h4><br>
                        <table class="table table-bordered table-responsive table-hover table-striped">
                            <thead>
                            <tr>
                                <th>Serial</th>
                                <th>Category Name</th>
                                <th>Category Description</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            $serial=1;
                            foreach ($allData as $oneData){
                                echo "<tr>
                                        <td> $serial</td>
                                        <td> $oneData->category_name</td>
                                        <td> $oneData->category_desc</td>
                                        <td> <a href='editCategory.php?id=$oneData->id' class='btn btn-primary'>Edit</a></td>
                                    </tr>";
                                $serial++;
                            }
                            ?>

                            </tbody>
                        </table>
                    </div>
                </div>

            </div>
            <!--inner block end here-->
            <?php include_once("templateLayout/footer.php");?>
        </div>

        <!--slider menu-->
        <?php include_once("templateLayout/navigation.php");?>
        <div class="clearfix"> </div>
    </div>
    <!--slide bar menu end here-->
    <?php include_once("templateLayout/script.php");?>
    </body>
    </html>




<?php
/**
 * Created by PhpStorm.
 * User: Dipa Das
 * Date: 18/10/2017
 * Time: 01:16
 */