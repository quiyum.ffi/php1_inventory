<?php
session_start();
include_once("templateLayout/templateInfo.php");
if($_SESSION['status']==1){
    include_once "../src/Login_info.php";
    $authentication=new Login_info();
    $authentication->prepareData($_SESSION);
    $check=$authentication->logged_in();
    if(!$check){
        $_SESSION['message']="Please Login First";
        header("Location: login.php");
    }
}
else{
    $_SESSION['message']="Please Login First";
    header("Location: login.php");
}
include_once "../src/Bill_master.php";
include_once "../src/Bill_details.php";
include_once "../src/Pay_bill.php";
$purBillObject=new Pay_bill();
$bMasterObject=new Bill_master();
$bObject=new Bill_details();
$bMasterObject->prepareData($_GET);
$bObject->prepareData($_GET);
$purBillObject->prepareData($_GET);
$masterData=$bMasterObject->showOneDetails();
$details=$bObject->showDetails();
$purBillDetails=$purBillObject->showBill();
$date=date("d-M-Y",strtotime($masterData->date));
$time=date("h:i a",strtotime($masterData->date));

?>
<!DOCTYPE HTML>
<html>
<head>
    <title><?php echo $title;?></title>
    <?php include_once("templateLayout/css.php");?>
</head>
<body>
<div class="page-container">
    <div class="left-content">
        <?php include_once("templateLayout/header.php") ?>
        <!-- script-for sticky-nav -->
        <?php include_once("templateLayout/script.php") ?>
        <!-- /script-for sticky-nav -->
        <!--inner block start here-->
        <div class="inner-block" style="min-height: 700px">
            <div class="row">
                <div class="col-md-12 col-md-offset-0 col-sm-12 col-sm-offset-0 col-xs-12">
                    <p class="text-center"><?php if(isset($_SESSION['message'])){echo $_SESSION['message']; $_SESSION['message']="";} ?></p>
                    <div class="login-block">
                        <div class="row">
                            <div class="col-md-2">
                                <label>MRR No</label>
                                <input type="text" class="form-control" value="<?php echo $masterData->id; ?>  ">
                            </div>
                            <div class="col-md-2">
                                <label>Date</label>
                                <input type="text" class="form-control" value="<?php echo $date;?> ">
                            </div>
                            <div class="col-md-2">
                                <label>Time</label>
                                <input type="text" class="form-control" value="<?php echo $time; ?> ">
                            </div>

                            <div class="col-md-3">
                                <label>Customer Name</label>
                                <input type="text" class="form-control" value=" <?php echo $masterData->customer_name ?>">
                            </div>
                            <div class="col-md-3">
                                <label>Customer Contact</label>
                                <input type="text" class="form-control" value="<?php echo $masterData->customer_contact ?>">
                            </div>
                            <div class="col-md-2">
                                <label>Total</label>
                                <input type="text" class="form-control" value="<?php echo $masterData->total; ?> ">
                            </div>

                            <div class="col-md-2">
                                <label>Payment</label>
                                <input type="text" class="form-control" value="<?php echo $masterData->payment ?>">
                            </div>
                            <div class="col-md-2">
                                <label>Due</label>
                                <input type="text" class="form-control" value="<?php echo $masterData->due ?>">
                            </div>
                            <?php
                            if($masterData->due>0){
                                ?>
                                <form action="../controller/newPayBill.php" method="post">
                                    <div class="col-md-3">
                                        <label>New Payment</label>
                                        <input type="number" class="form-control" name="new_payment" placeholder="Input Your Payment" max="<?php echo $masterData->due;?>">
                                    </div>
                                    <input type="hidden" name="total" value="<?php echo $masterData->total;?>">
                                    <input type="hidden" name="payment" value="<?php echo $masterData->payment;?>">
                                    <input type="hidden" name="id" value="<?php echo $masterData->id;?>">

                                    <div class="col-md-2">
                                        <label></label>
                                        <input type="submit" class="btn btn-primary" value="Pay Your Slip">
                                    </div>
                                </form>
                                <?php
                            }?>
                        </div>
                        <br><hr>
                        <div>
                            <div class="col-md-7">
                                <h3 class="text-center">Purchase Details</h3>
                                <table id="example" class="table table-bordered" >
                                    <thead>
                                    <tr>
                                        <th>Serial</th>
                                        <th>Product</th>
                                        <th>Quantity</th>
                                        <th>Unit</th>
                                        <th>Price</th>
                                        <th>Total Price</th>

                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                    $serial=1;
                                    foreach($details as $oneData){
                                        ?>

                                        <tr>
                                            <td><?php echo $serial;?></td>
                                            <td><?php echo $oneData->product_name;?></td>
                                            <td><?php echo $oneData->quantity;?></td>
                                            <td><?php echo $oneData->unit_name;?></td>
                                            <td><?php echo $oneData->price;?></td>
                                            <td><?php echo $oneData->total_price;?></td>
                                        </tr>
                                        <?php
                                        $serial++;
                                    }
                                    ?>

                                    </tbody>
                                </table> <br>
                            </div>
                            <div class="col-md-5">
                                <h3 class="text-center">Purchase Bill Details</h3>
                                <table id="example2" class="table table-bordered" >
                                    <thead>
                                    <tr>
                                        <th>Serial</th>
                                        <th>Date</th>
                                        <th>Time</th>
                                        <th>Amount</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                    $serial=1;
                                    foreach($purBillDetails as $oneData){
                                        $newDate=date("d-M-Y",strtotime($oneData->date));
                                        $newTime=date("h:i a",strtotime($oneData->date));

                                        ?>

                                        <tr>
                                            <td><?php echo $serial;?></td>
                                            <td><?php echo $newDate;?></td>
                                            <td><?php echo $newTime;?></td>
                                            <td><?php echo $oneData->amount;?></td>
                                        </tr>
                                        <?php
                                        $serial++;
                                    }
                                    ?>

                                    </tbody>
                                </table> <br>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
        <!--inner block end here-->
        <?php include_once("templateLayout/footer.php");?>
    </div>

    <!--slider menu-->
    <?php include_once("templateLayout/navigation.php");?>
    <div class="clearfix"> </div>
</div>
<!--slide bar menu end here-->
<?php include_once("templateLayout/script.php");?>
</body>
</html>




