<?php
include_once("templateLayout/templateInfo.php");
include_once "../src/Unit_lookup.php";
$object=new Unit_lookup();
$object->prepareData($_GET);
$allData=$object->showUnit();
$oneUnit=$object->showOneUnit();

?>
<!DOCTYPE HTML>
<html>
<head>
    <title><?php echo $title;?></title>
    <?php include_once("templateLayout/css.php");?>
</head>
<body>
<div class="page-container">
    <div class="left-content">
        <?php include_once("templateLayout/header.php") ?>
        <!-- script-for sticky-nav -->
        <?php include_once("templateLayout/script.php") ?>
        <!-- /script-for sticky-nav -->
        <!--inner block start here-->
        <div class="inner-block" style="min-height: 700px">
            <div class="row">
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <div class="login-block">

                        <form action="../controller/updateUnit.php" method="post">
                            <input type="text" name="unit_name" value="<?php echo $oneUnit->unit_name; ?> " required="">
                            <input type="hidden" name="id" value="<?php echo $oneUnit->id;?>">

                            <br>
                            <input type="submit" value="Update Unit">
                        </form>
                    </div>
                </div>
                <div class="col-md-6 col-sm-6 col-xs-12" style="margin-top: 23px">
                    <h4 class="text-center">Unit Lookup Table</h4><br>
                    <table class="table table-bordered table-responsive table-hover table-striped">
                        <thead>
                        <tr>
                            <th>Serial</th>
                            <th>Unit</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        $serial=1;
                        foreach ($allData as $oneData){
                            echo "<tr>
                                        <td> $serial</td>
                                        <td> $oneData->unit_name</td>
                                        <td><a href='editUnit.php?id=$oneData->id' class='btn btn-primary'>Edit</a></td>
                                    </tr>";
                            $serial++;
                        }
                        ?>

                        </tbody>
                    </table>
                </div>
            </div>

        </div>
        <!--inner block end here-->
        <?php include_once("templateLayout/footer.php");?>
    </div>

    <!--slider menu-->
    <?php include_once("templateLayout/navigation.php");?>
    <div class="clearfix"> </div>
</div>
<!--slide bar menu end here-->
<?php include_once("templateLayout/script.php");?>
</body>
</html>
