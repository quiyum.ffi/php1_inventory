<?php
include_once 'Database.php';
class Unit_lookup extends Database
{
    public $id;
    public $unit_name; 
   
   

    public function __construct(){

        parent:: __construct();
    }
    public function prepareData($data){
        if(array_key_exists('id',$data)){
            $this->id=$data['id'];
        }
    
        if(array_key_exists('unit_name',$data)){
            $this->unit_name=$data['unit_name'];
        }
        return $this;
    }
    public function insertUnit(){
        $query= "INSERT INTO `unit_lookup`(`unit_name`) VALUES (?)";
        $STH = $this->DBH->prepare($query);
        $STH->bindParam(1,$this->unit_name);
        $STH->execute();
    }
    public function showUnit(){
        $sql = "SELECT * FROM unit_lookup";
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetchAll();
    }
    public function showOneUnit(){
        $sql = "SELECT * FROM unit_lookup WHERE id=$this->id";
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetch();
    }
    public function updateUnit(){
        $query="UPDATE unit_lookup SET unit_name=? WHERE id=$this->id";
        $STH=$this->DBH->prepare($query);
        $STH->bindParam(1,$this->unit_name);
        $STH->execute();
    }
}