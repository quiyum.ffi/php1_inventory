<?php
include_once "Database.php";
class Purchase_bill extends Database
{
    public $id;
    public $purchase_master_id;
    public $date;
    public $amount;
    public $payment; 

    public function __construct(){

        parent:: __construct();
    }
    public function prepareData($data){
        if(array_key_exists("id",$data)){
            $this->id=$data['id'];
        }
        if(array_key_exists("new_payment",$data)){
            $this->amount=$data['new_payment'];
        }
        if(array_key_exists("payment",$data)){
            $this->payment=$data['payment'];
        }
        
        return $this;
    }
    public function insertData(){
        $select_master_id="SELECT id FROM `purchase_master` ORDER BY id DESC LIMIT 1";
        $STH=$this->DBH->query($select_master_id);
        $STH->setFetchMode(PDO::FETCH_ASSOC);
        $STH->execute();
        $row=$STH->fetch();
        $this->purchase_master_id=$row['id'];
        date_default_timezone_set('Asia/Dhaka');
        $date= date('Y-m-d H-i-s');
        $this->date=$date;

            $query= "INSERT INTO `purchase_bill`(`purchase_master_id`, `date`, `amount`) VALUES (?,?,?)";
            $STH = $this->DBH->prepare($query);
            $STH->bindParam(1,$this->purchase_master_id);
            $STH->bindParam(2,$this->date);
            $STH->bindParam(3,$this->payment);
            $STH->execute();
    }
    public function newInsertData(){
        date_default_timezone_set('Asia/Dhaka');
        $date= date('Y-m-d H-i-s');
        $this->date=$date;
        $query= "INSERT INTO `purchase_bill`(`purchase_master_id`, `date`, `amount`) VALUES (?,?,?)";
        $STH = $this->DBH->prepare($query);
        $STH->bindParam(1,$this->id);
        $STH->bindParam(2,$this->date);
        $STH->bindParam(3,$this->amount);
        $STH->execute();
    }
    public function showBill(){
        $sql = "SELECT * FROM purchase_bill WHERE purchase_master_id='$this->id' ORDER BY  id DESC ";
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetchAll();
    }
}