<?php

class Database
{
    public $DBH;
    public function __construct()
    {
        try{

            $this->DBH = new PDO('mysql:host=localhost;dbname=php1_inventory', "root", "");
            $this->DBH->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );

            return $this->DBH;
        }
        catch(PDOException $error){
            echo "Database Error: ". $error->getMessage();
        }
    }
}

