<?php
include_once "Database.php";

class Stock extends Database
{
    public $id;
    
    public function __construct(){

        parent:: __construct();
    }
    public function prepareData($data){
        if(array_key_exists("product_id",$data)){
            $this->id=$data['product_id'];
        } 
    }

    public function showStock(){
        $sql = "SELECT * FROM stock ";
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetchAll();
    }
    public function showOneStock(){
        $sql = "SELECT * FROM stock WHERE id='$this->id'";
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetch();
    }
}