<?php

include_once 'Database.php';
class Category extends Database
{
    public $id;
    public $category_name;
    public $category_desc;



    public function __construct(){

        parent:: __construct();
    }
    public function prepareData($data){
        if(array_key_exists('id',$data)){
            $this->id=$data['id'];
        }
        if(array_key_exists('category_name',$data)){
            $this->category_name=$data['category_name'];
        }
        if(array_key_exists('category_desc',$data)){
            $this->category_desc=$data['category_desc'];
        }
        return $this;
    }
    public function insertUnit(){
        $query= "INSERT INTO category(category_name,category_desc) VALUES (?,?)";
        $STH = $this->DBH->prepare($query);
        $STH->bindParam(1,$this->category_name);
        $STH->bindParam(2,$this->category_desc);
        $STH->execute();
    }
    public function showCategory(){
        $sql = "SELECT * FROM category";
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetchAll();
    }
    public function showOneCategory(){
        $sql = "SELECT * FROM category WHERE id=$this->id";
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetch();
    }
    public function updateCategory(){
        $query="UPDATE `category` SET `category_name`=?,`category_desc`=? WHERE id=$this->id";
        $STH=$this->DBH->prepare($query);
        $STH->bindParam(1,$this->category_name);
        $STH->bindParam(2,$this->category_desc);
        $STH->execute();
    }

}