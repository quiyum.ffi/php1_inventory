<?php

include_once 'Database.php';
class Product extends Database
{
    public $id;
    public $product_name;
    public $category_id;
    public $unit_id;




    public function __construct(){

        parent:: __construct();
    }
    public function prepareData($data){
        if(array_key_exists('id',$data)){
            $this->id=$data['id'];
        }
        if(array_key_exists('product_name',$data)){
            $this->product_name=$data['product_name'];
        }
        if(array_key_exists('category_id',$data)){
            $this->category_id=$data['category_id'];
        }
        if(array_key_exists('unit_id',$data)){
            $this->unit_id=$data['unit_id'];
        }
        return $this;
    }
    public function insertProduct(){
        $query= "INSERT INTO product(product_name,category_id,unit_id) VALUES (?,?,?)";
        $STH = $this->DBH->prepare($query);
        $STH->bindParam(1,$this->product_name);
        $STH->bindParam(2,$this->category_id);
        $STH->bindParam(3,$this->unit_id);
        $STH->execute();
    }
    public function showProduct(){
        $sql = "SELECT product.id,product.product_name,category.category_name,unit_lookup.unit_name FROM `product`,category,unit_lookup WHERE product.category_id=category.id AND product.unit_id=unit_lookup.id";
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetchAll();
    }
    public function showOneProduct(){
        $sql = "SELECT * FROM product WHERE id=$this->id";
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetch();
    }
    public function updateProduct(){
        $query="UPDATE `product` SET `product_name`=?,`category_id`=?,`unit_id`=? WHERE id=$this->id";
        $STH=$this->DBH->prepare($query);
        $STH->bindParam(1,$this->product_name);
        $STH->bindParam(2,$this->category_id);
        $STH->bindParam(3,$this->unit_id);
        $STH->execute();
    }
    public function if_exist(){
        $sql = "SELECT * FROM product WHERE product_name='$this->product_name' AND category_id='$this->category_id' AND unit_id='$this->unit_id'";
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        $STH->fetch();
        $count=$STH->rowCount();

        if($count){
            return TRUE;
        }
        else{
            return FALSE;
        }
    }
    public function deleteProduct(){
        $query = "DELETE FROM product WHERE id=$this->id";
        $STH = $this->DBH->exec($query);
    }

}