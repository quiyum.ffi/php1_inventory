<?php
session_start();
include_once ("../src/Login_info.php");
$obj=new Login_info();
$obj->prepareData($_POST);
$check=$obj->login();
$data=$obj->user_info();
if($check){
    $_SESSION['status']=1;
    $_SESSION['user_id']=$data->id;
    $_SESSION['email']=$data->email;
    $_SESSION['message']="Welcome!";
    header("Location: ../views/index.php");
}
else{
    $_SESSION['message']="Email & Password doesn't match";
    header("Location: ../views/login.php");
}