-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Nov 04, 2017 at 07:08 AM
-- Server version: 10.2.3-MariaDB-log
-- PHP Version: 7.1.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `php1_inventory`
--

-- --------------------------------------------------------

--
-- Table structure for table `billtemp`
--

CREATE TABLE `billtemp` (
  `id` int(11) NOT NULL,
  `customer_name` varchar(255) NOT NULL,
  `customer_contact` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  `price` int(11) NOT NULL,
  `total_price` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `billtemp`
--

INSERT INTO `billtemp` (`id`, `customer_name`, `customer_contact`, `product_id`, `quantity`, `price`, `total_price`) VALUES
(5, 'fdfdf', 3434434, 19, 20, 25, 500);

-- --------------------------------------------------------

--
-- Table structure for table `bill_details`
--

CREATE TABLE `bill_details` (
  `id` int(11) NOT NULL,
  `bill_master_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  `price` int(11) NOT NULL,
  `total_price` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `bill_details`
--

INSERT INTO `bill_details` (`id`, `bill_master_id`, `product_id`, `quantity`, `price`, `total_price`) VALUES
(1, 4, 13, 23, 2112, 48576),
(2, 4, 18, 32, 3255, 104160),
(3, 5, 13, 24, 352, 8448),
(4, 6, 13, 234, 1543, 361062),
(5, 7, 13, 34, 34324, 1167016),
(6, 9, 14, 43, 40, 1720),
(7, 10, 14, 10, 100, 1000);

-- --------------------------------------------------------

--
-- Table structure for table `bill_master`
--

CREATE TABLE `bill_master` (
  `id` int(11) NOT NULL,
  `date` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `customer_name` varchar(255) NOT NULL,
  `customer_contact` int(11) NOT NULL,
  `total` int(11) NOT NULL,
  `payment` int(11) NOT NULL,
  `due` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `bill_master`
--

INSERT INTO `bill_master` (`id`, `date`, `customer_name`, `customer_contact`, `total`, `payment`, `due`) VALUES
(1, '2017-11-03 19:53:44', 'pran', 2147483647, 152736, 24252, 128484),
(2, '2017-11-03 20:00:30', 'pran', 2147483647, 152736, 52736, 100000),
(3, '2017-11-03 20:05:17', 'pran', 2147483647, 152736, 25255, 127481),
(4, '2017-11-03 20:18:02', 'pran', 2147483647, 152736, 53708, 99028),
(5, '2017-11-03 20:27:32', 'pran', 2147483647, 8448, 6145, 2303),
(6, '2017-11-04 05:16:37', 'lopini', 2147483647, 361062, 56355, 304707),
(7, '2017-11-04 05:19:47', 'happy', 2147483647, 1167016, 167016, 1000000),
(8, '2017-11-04 06:43:53', 'dfdfd', 45353, 1720, 1720, 0),
(9, '2017-11-04 06:47:08', 'dfdfd', 45353, 1720, 1720, 0),
(10, '2017-11-04 06:49:14', 'sdsd', 34343, 1000, 1000, 0);

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE `category` (
  `id` int(11) NOT NULL,
  `category_name` varchar(255) NOT NULL,
  `category_desc` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`id`, `category_name`, `category_desc`) VALUES
(10, 'grocery', 'gygtdksetxezycrviybulynbuyvrcdsted'),
(11, 'hardware', 'nejklkcfnrmekncfkemde'),
(12, 'electronics', 'ejrnclkdmsilmrilefncvjfncm'),
(13, 'cell phone', 'cdsjcnjkndfvujfkcndm ');

-- --------------------------------------------------------

--
-- Stand-in structure for view `inventory`
-- (See below for the actual view)
--
CREATE TABLE `inventory` (
`p_id` int(11)
,`debit_quantity` bigint(20)
,`credit_quantity` bigint(11)
);

-- --------------------------------------------------------

--
-- Table structure for table `pay_bill`
--

CREATE TABLE `pay_bill` (
  `id` int(11) NOT NULL,
  `bill_master_id` int(11) NOT NULL,
  `date` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `amount` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `pay_bill`
--

INSERT INTO `pay_bill` (`id`, `bill_master_id`, `date`, `amount`) VALUES
(1, 4, '2017-11-04 06:47:00', 52736),
(2, 4, '2017-11-04 06:47:00', 52736),
(3, 4, '2017-11-04 06:47:00', 53060),
(4, 4, '2017-11-04 06:47:00', 53384),
(5, 5, '2017-11-04 06:47:00', 2445),
(6, 5, '2017-11-04 06:47:00', 2445),
(7, 5, '2017-11-04 06:47:00', 5445),
(8, 6, '2017-11-04 06:47:00', 56355),
(9, 7, '2017-11-04 06:47:00', 167016),
(10, 9, '2017-11-04 06:47:08', 1720),
(11, 10, '2017-11-04 06:49:14', 1000);

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE `product` (
  `id` int(11) NOT NULL,
  `product_name` varchar(255) NOT NULL,
  `category_id` int(11) NOT NULL,
  `unit_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `product`
--

INSERT INTO `product` (`id`, `product_name`, `category_id`, `unit_id`) VALUES
(13, 'samsung ', 13, 32),
(14, 'rice', 10, 29),
(15, 'oil', 10, 31),
(16, 'salt', 10, 30),
(17, 'samsung tv', 11, 32),
(18, 'sony', 12, 32),
(19, 'potato', 10, 30),
(20, 'Hauwai', 13, 32),
(21, 'dal', 10, 30),
(22, 'oil', 10, 31);

-- --------------------------------------------------------

--
-- Table structure for table `purchase_bill`
--

CREATE TABLE `purchase_bill` (
  `id` int(11) NOT NULL,
  `purchase_master_id` int(11) NOT NULL,
  `date` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `amount` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `purchase_bill`
--

INSERT INTO `purchase_bill` (`id`, `purchase_master_id`, `date`, `amount`) VALUES
(1, 36, '2017-11-03 15:30:55', 34245),
(2, 22, '2017-11-03 16:10:26', 128531),
(3, 22, '2017-11-03 16:28:28', 132765),
(4, 22, '2017-11-03 16:28:41', 175117),
(5, 22, '2017-11-03 16:31:49', 199642),
(6, 22, '2017-11-03 16:35:39', 200287),
(7, 22, '2017-11-03 16:36:33', 2000),
(8, 42, '2017-11-03 20:11:08', 76876),
(9, 24, '2017-11-03 20:13:07', 675654),
(10, 24, '2017-11-03 20:13:16', 1128209),
(11, 24, '2017-11-03 20:13:36', 1153442),
(12, 24, '2017-11-03 20:14:18', 2000),
(13, 44, '2017-11-03 20:22:33', 45234),
(14, 27, '2017-11-03 20:22:44', 2465153),
(15, 27, '2017-11-03 20:22:50', 2515153),
(16, 28, '2017-11-03 20:59:20', 534543),
(17, 28, '2017-11-03 20:59:29', 4352),
(18, 27, '2017-11-04 05:01:12', 45345),
(19, 52, '2017-11-04 05:06:01', 35245),
(20, 27, '2017-11-04 05:06:08', 6534),
(21, 53, '2017-11-04 06:32:15', 1200);

-- --------------------------------------------------------

--
-- Table structure for table `purchase_details`
--

CREATE TABLE `purchase_details` (
  `id` int(11) NOT NULL,
  `purchase_master_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  `price` int(11) NOT NULL,
  `total_price` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `purchase_details`
--

INSERT INTO `purchase_details` (`id`, `purchase_master_id`, `product_id`, `quantity`, `price`, `total_price`) VALUES
(1, 2, 16, 10, 20, 200),
(2, 2, 21, 30, 50, 1500),
(3, 2, 16, 5, 50, 250),
(4, 2, 18, 26, 15000, 390000),
(5, 2, 13, 23, 14000, 322000),
(6, 3, 16, 10, 20, 200),
(7, 3, 21, 30, 50, 1500),
(8, 3, 16, 5, 50, 250),
(9, 3, 18, 26, 15000, 390000),
(10, 3, 13, 23, 14000, 322000),
(11, 4, 19, 45, 34, 1530),
(12, 4, 16, 34, 67, 2278),
(13, 4, 22, 23, 105, 2415),
(14, 5, 18, 34, 569900, 19376600),
(15, 6, 13, 43, 56666, 2436638),
(16, 6, 20, 37, 46356, 1715172),
(17, 7, 0, 0, 0, 0),
(18, 8, 20, 67, 65657, 4399019),
(19, 9, 20, 45, 43532, 1958940),
(20, 10, 13, 65, 56553, 3675945),
(21, 11, 20, 5, 57678, 288390),
(22, 12, 13, 34, 43456, 1477504),
(23, 13, 0, 0, 0, 0),
(24, 14, 13, 45, 33445, 1505025),
(25, 15, 13, 23, 4532, 104236),
(26, 15, 20, 12, 3253, 39036),
(27, 16, 13, 23, 2343, 53889),
(28, 16, 20, 43, 2342, 100706),
(29, 16, 17, 33, 2342, 77286),
(30, 16, 18, 23, 2321, 53383),
(31, 17, 13, 43, 3453, 148479),
(32, 17, 17, 34, 3432, 116688),
(33, 17, 20, 54, 3443, 185922),
(34, 17, 18, 43, 4325, 185975),
(35, 18, 0, 0, 0, 0),
(36, 19, 0, 0, 0, 0),
(37, 20, 13, 23, 3242, 74566),
(38, 20, 18, 32, 23543, 753376),
(39, 21, 13, 54, 43453, 2346462),
(40, 21, 18, 23, 36432, 837936),
(41, 21, 17, 32, 4745, 151840),
(42, 22, 14, 43, 2345, 100835),
(43, 22, 21, 24, 4325, 103800),
(44, 22, 15, 54, 243, 13122),
(45, 23, 13, 23, 35, 805),
(46, 23, 18, 32, 34, 1088),
(47, 24, 13, 45, 34454, 1550430),
(48, 24, 18, 42, 34543, 1450806),
(49, 24, 17, 23, 34524, 794052),
(50, 25, 13, 56, 4657, 260792),
(51, 25, 18, 43, 4355, 187265),
(52, 26, 13, 56, 4657, 260792),
(53, 26, 18, 43, 4355, 187265),
(54, 35, 13, 56, 4657, 260792),
(55, 35, 18, 43, 4355, 187265),
(56, 35, 20, 34, 353444, 12017096),
(57, 36, 13, 34, 3232, 109888),
(58, 36, 20, 23, 3254, 74842),
(59, 36, 18, 34, 2654, 90236),
(60, 42, 13, 45, 5456, 245520),
(61, 44, 13, 45, 45454, 2045430),
(62, 52, 13, 43, 34443, 1481049),
(63, 53, 16, 20, 60, 1200);

-- --------------------------------------------------------

--
-- Table structure for table `purchase_master`
--

CREATE TABLE `purchase_master` (
  `id` int(11) NOT NULL,
  `date` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `vendor_name` varchar(255) NOT NULL,
  `vendor_contact` varchar(255) NOT NULL,
  `total` int(11) NOT NULL,
  `payment` int(11) NOT NULL,
  `due` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `purchase_master`
--

INSERT INTO `purchase_master` (`id`, `date`, `vendor_name`, `vendor_contact`, `total`, `payment`, `due`) VALUES
(21, '2017-11-01 16:06:07', 'Pran', '018466575773', 3336238, 3336238, 0),
(22, '2017-11-03 16:36:33', 'Pran', '018466575773', 217757, 204287, 13470),
(23, '2017-11-02 04:42:56', 'Pran', '018466575773', 1893, 1893, 0),
(24, '2017-11-03 20:14:18', 'Pran', '018466575773', 3795288, 1158442, 2636846),
(25, '2017-11-03 14:54:37', 'Pran', '018466575773', 448057, 35735, 412322),
(26, '2017-11-03 14:56:18', 'Pran', '018466575773', 448057, 56766, 391291),
(27, '2017-11-04 05:06:08', 'Pran', '018466575773', 12465153, 3131387, 9333766),
(28, '2017-11-03 20:59:29', 'Pran', '018466575773', 12465153, 3004048, 9461105),
(29, '2017-11-03 15:09:32', 'Pran', '018466575773', 12465153, 2465153, 10000000),
(30, '2017-11-03 15:09:46', 'Pran', '018466575773', 12465153, 2465153, 10000000),
(31, '2017-11-03 15:12:04', 'Pran', '018466575773', 12465153, 465153, 12000000),
(32, '2017-11-03 15:14:37', 'Pran', '018466575773', 12465153, 465153, 12000000),
(33, '2017-11-03 15:56:44', 'Pran', '018466575773', 12465153, 105756, 12359397),
(34, '2017-11-03 15:17:11', 'Pran', '018466575773', 12465153, 546766, 11918387),
(35, '2017-11-03 15:17:51', 'Pran', '018466575773', 12465153, 35646, 12429507),
(36, '2017-11-03 15:30:55', 'Pran', '018466575773', 274966, 34245, 240721),
(37, '2017-11-03 18:13:19', 'Pran', '018466575773', 245520, 53465, 192055),
(38, '2017-11-03 18:15:14', 'Pran', '018466575773', 245520, 34352, 211168),
(39, '2017-11-03 20:00:50', 'Pran', '018466575773', 245520, 45520, 200000),
(40, '2017-11-03 20:03:38', 'Pran', '018466575773', 245520, 24552, 220968),
(41, '2017-11-03 20:09:55', 'Pran', '018466575773', 245520, 75555, 169965),
(42, '2017-11-03 20:11:08', 'Pran', '018466575773', 245520, 76876, 168644),
(43, '2017-11-03 20:15:10', 'Pran', '018466575773', 2045430, 63565, 1981865),
(44, '2017-11-03 20:22:33', 'Pran', '018466575773', 2045430, 45234, 2000196),
(45, '2017-11-03 20:23:33', 'Pran', '018466575773', 1481049, 4524, 1476525),
(46, '2017-11-03 20:24:40', 'Pran', '018466575773', 1481049, 4524, 1476525),
(47, '2017-11-03 20:25:39', 'Pran', '018466575773', 1481049, 35654, 1445395),
(48, '2017-11-03 20:41:42', 'Pran', '018466575773', 1481049, 45745, 1435304),
(49, '2017-11-04 04:54:49', 'Pran', '018466575773', 1481049, 4646, 1476403),
(50, '2017-11-04 05:01:19', 'Pran', '018466575773', 1481049, 43254, 1437795),
(51, '2017-11-04 05:01:51', 'Pran', '018466575773', 1481049, 34565, 1446484),
(52, '2017-11-04 05:06:01', 'Pran', '018466575773', 1481049, 35245, 1445804),
(53, '2017-11-04 06:32:15', 'pran', '343432434', 1200, 1200, 0);

-- --------------------------------------------------------

--
-- Stand-in structure for view `stock`
-- (See below for the actual view)
--
CREATE TABLE `stock` (
`id` int(11)
,`product_name` varchar(255)
,`debit_quantity` decimal(41,0)
,`credit_quantity` decimal(41,0)
,`balance_quantity` decimal(42,0)
);

-- --------------------------------------------------------

--
-- Table structure for table `temp`
--

CREATE TABLE `temp` (
  `id` int(11) NOT NULL,
  `vendor_name` varchar(255) NOT NULL,
  `vendor_contact` varchar(255) NOT NULL,
  `product_id` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  `price` int(11) NOT NULL,
  `total_price` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `unit_lookup`
--

CREATE TABLE `unit_lookup` (
  `id` int(11) NOT NULL,
  `unit_name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `unit_lookup`
--

INSERT INTO `unit_lookup` (`id`, `unit_name`) VALUES
(29, 'gram'),
(30, 'kilogram'),
(31, 'litre'),
(32, 'pcs'),
(33, 'meter');

-- --------------------------------------------------------

--
-- Structure for view `inventory`
--
DROP TABLE IF EXISTS `inventory`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `inventory`  AS  select `purchase_details`.`product_id` AS `p_id`,`purchase_details`.`quantity` AS `debit_quantity`,0 AS `credit_quantity` from `purchase_details` union all select `bill_details`.`product_id` AS `p_id`,0 AS `debit_quantity`,`bill_details`.`quantity` AS `credit_quantity` from `bill_details` ;

-- --------------------------------------------------------

--
-- Structure for view `stock`
--
DROP TABLE IF EXISTS `stock`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `stock`  AS  select `product`.`id` AS `id`,`product`.`product_name` AS `product_name`,sum(`inventory`.`debit_quantity`) AS `debit_quantity`,sum(`inventory`.`credit_quantity`) AS `credit_quantity`,sum(`inventory`.`debit_quantity`) - sum(`inventory`.`credit_quantity`) AS `balance_quantity` from (`inventory` join `product`) where `inventory`.`p_id` = `product`.`id` group by `inventory`.`p_id` ;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `billtemp`
--
ALTER TABLE `billtemp`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bill_details`
--
ALTER TABLE `bill_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bill_master`
--
ALTER TABLE `bill_master`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pay_bill`
--
ALTER TABLE `pay_bill`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `purchase_bill`
--
ALTER TABLE `purchase_bill`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `purchase_details`
--
ALTER TABLE `purchase_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `purchase_master`
--
ALTER TABLE `purchase_master`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `temp`
--
ALTER TABLE `temp`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `unit_lookup`
--
ALTER TABLE `unit_lookup`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `billtemp`
--
ALTER TABLE `billtemp`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `bill_details`
--
ALTER TABLE `bill_details`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `bill_master`
--
ALTER TABLE `bill_master`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `pay_bill`
--
ALTER TABLE `pay_bill`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `product`
--
ALTER TABLE `product`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;
--
-- AUTO_INCREMENT for table `purchase_bill`
--
ALTER TABLE `purchase_bill`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;
--
-- AUTO_INCREMENT for table `purchase_details`
--
ALTER TABLE `purchase_details`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=64;
--
-- AUTO_INCREMENT for table `purchase_master`
--
ALTER TABLE `purchase_master`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=54;
--
-- AUTO_INCREMENT for table `temp`
--
ALTER TABLE `temp`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `unit_lookup`
--
ALTER TABLE `unit_lookup`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
